# cc3172

浪西游

windouws平台：Visual Studio 项目文件不见了，只剩下一个exe...

android平台：需要把langxiyou/复制到手机，并给apk读写手机存储权限（直读wdf和map）

半成品，只能逛逛...

# libcc

A星寻路

资源直读

# libcocos2d

cocos2dx-v3.172 精简框架

# libpro

装备 物品 宠物 技能 属性 属性点 修炼 资质

染色系统

# libtxt

门派 模型表 动作表 物品表 地图表 技能表 资质表

# libui

各个面板/界面

# libutil

按钮 丰富文本 标签 精灵 提示框

地图直读与渲染系统

# 示意图

![apk](https://gitee.com/ulxy/cc3172/raw/master/diagram/apk.jpg)

![android](https://gitee.com/ulxy/cc3172/raw/master/diagram/android.jpg)

![transmit](https://gitee.com/ulxy/cc3172/raw/master/diagram/transmit.png)

![iter](https://gitee.com/ulxy/cc3172/raw/master/diagram/iter.png)

![ski](https://gitee.com/ulxy/cc3172/raw/master/diagram/ski.png)

![baby1](https://gitee.com/ulxy/cc3172/raw/master/diagram/baby1.png)

![baby2](https://gitee.com/ulxy/cc3172/raw/master/diagram/baby2.png)

![baby3](https://gitee.com/ulxy/cc3172/raw/master/diagram/baby3.png)

![baby4](https://gitee.com/ulxy/cc3172/raw/master/diagram/baby4.png)


# 开发环境（windows）

Visual Studio Community 2017
![windows](https://gitee.com/ulxy/diagram_images/raw/master/installer.png)


# 开发环境（android）

Android Studio